export const my_length_array = (arr) => {
    let a = 0;
    while(arr && arr[a]) {
        a++;
    }
    return a;
};