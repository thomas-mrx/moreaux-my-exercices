export const my_size_alpha = (str) => {
    let c = 0;
    if(str && typeof str === "string"){
        while(str[c]) {
            c++;
        }
    }
    return c;
};