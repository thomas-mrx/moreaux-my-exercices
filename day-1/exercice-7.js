export const my_is_posi_neg = (nbr) => {
    let res = "POSITIF";
    if(typeof nbr === "number") {
        if(nbr < 0) {
            res = "NEGATIF";
        } else if(nbr === 0) {
            res = "NEUTRAL";
        }
    }
    return res;
};